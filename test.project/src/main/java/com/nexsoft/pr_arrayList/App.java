package com.nexsoft.pr_arrayList;

import java.util.*;

import com.nexsoft.pr.app;
// import com.nexsoft.testSuper.MyCar;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {

        // app dapetkan = new app();
        // dapetkan.dapet();

        // System.out.println("Hello World");

        ArrayList<String> teman = new ArrayList<>();
        teman.add("Risa");
        teman.add("Liana");
        teman.add("Adit");

        ArrayList<String> temanDekat = new ArrayList<>();
        temanDekat.add("Risa");
        temanDekat.add("Liana");
        temanDekat.add("Gilas");
        temanDekat.add("Aldo");

        ArrayList<String> temanku = new ArrayList<>(teman);
        ArrayList<String> temanDekatku = new ArrayList<>(temanDekat);

        temanku.retainAll(temanDekatku);
        temanDekatku.removeAll(temanku);

        teman.addAll(temanDekatku);
        teman.removeAll(temanku);
        System.out.println(teman);

    }
}

// public static void firstRun() {
// System.out.println("My First Run with Bus");
// }
// teman.retainAll(temanDekat);
// temanDekat.removeAll(teman);
// if (teman.containsAll(temanDekat) == false) {

// teman.addAll(temanDekat);
// System.out.println(teman);
// }

// temanDekat.addAll(teman);

// if (teman.retainAll(temanDekat) == true) {
// temanDekat.removeAll(teman);
// teman.clear();
// if (teman.isEmpty() == true) {

// System.out.println(temanDekat);
// }
// }

// if (temanDekat.retainAll(teman) == true) {

// // teman.removeAll(temanDekat);
// teman.removeAll(teman);
// teman.addAll(temanDekat);
// System.out.println(teman);

// }
// Iterator itrtemanDekat = temanDekat.iterator();
// while (itrtemanDekat.hasNext()) {
// }

// }

// teman.clear();
// if (teman.isEmpty()==true) {
// teman.add("Gilas");
// teman.add("Aldo");
// }
// System.out.println(teman);

// firstRun();

/* JANGAN HIRAUKAN KOMEN INI */

// System.out.println("Hello World!");

// MyCar myDetail = new MyCar();
// myDetail.detail();
// }

// public void fuel() {
// System.out.println("Fuel : Solar");
// }

// public void color() {
// System.out.println("Color : Black");
// }
