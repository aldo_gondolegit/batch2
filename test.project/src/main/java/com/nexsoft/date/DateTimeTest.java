package com.nexsoft.date;
import java.util.*;
import java.text.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateTimeTest {
    
    
    public void showDate(){

        
        DateTimeFormatter formatTanggalLahirkuu = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String tanggalLahir = "19-08-2003";
        LocalDate  tanggalLahirku = LocalDate.parse(tanggalLahir,formatTanggalLahirkuu);

        DateTimeFormatter formatTanggalLahirku = DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy");
        LocalDate tanggalSekarang = LocalDate.now();
        
        String tanggal = tanggalLahirku.format(formatTanggalLahirku);
        String tanggalSkr = tanggalSekarang.format(formatTanggalLahirku);

        System.out.println(tanggalSkr);
        System.out.println("____________________ \n" );
        System.out.println(tanggal);


        Long selisih = ChronoUnit.DAYS.between(tanggalLahirku,tanggalSekarang);
        System.out.println("____________________ \n" );
        System.out.println("Selisih " + selisih + " Hari");
        System.out.println("____________________ \n" );
        
    }
    
}
        // if (formatTanggalLahirku == Locale.ENGLISH) {
            //     Locale id = new Locale("id","ID");
            // }
// System.out.println(tanggalLahirku);

        // DateTimeFormatter formatTanggalTujuan = DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy",Locale.forLanguageTag("id-ID"));
        // String tanggalTujuan = tanggalSekarang.format(formatTanggalTujuan);

        
        //INI VERSI BUKAN STRING
      /*  LocalDate tanggalLahir = LocalDate.of(2003, 8, 19);
        DateTimeFormatter formatTanggal = DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy",Locale.forLanguageTag("id-ID"));
        String tanggal = tanggalLahir.format(formatTanggal);

        LocalDate tanggalSekarang = LocalDate.now();
        DateTimeFormatter formatTanggalTujuan = DateTimeFormatter.ofPattern("EEEE dd MMMM yyyy",Locale.forLanguageTag("id-ID"));
        String tanggalTujuan = tanggalSekarang.format(formatTanggalTujuan);


        Long selisih = ChronoUnit.DAYS.between(tanggalLahir,tanggalSekarang);
        System.out.println(tanggal);
        System.out.println(selisih + " Hari");
        */

        // System.out.println(tanggal);
        // System.out.println(tanggalTujuan);



//创建一个代表2009年6月12日的Calendar对象
    //     Calendar c1 = Calendar.getInstance();
    //     c1.set(2009, 6 - 1, 12);
    //     System.out.println(c1);
    // }
        
    //     try {
    //         long start = System.currentTimeMillis( );
    //         System.out.println(new Date( ) + "\n");
    //         Thread.sleep(5*60*10);
    //         System.out.println(new Date( ) + "\n");
    //         long end = System.currentTimeMillis( );
    //         long diff = end - start;
    //         System.out.println("Perbedaannya adalah : " + diff);
    //      } catch (Exception e) {
    //         System.out.println("Terdapat exception!");
    //      }
    //   }

            // SimpleDateFormat sdf = new SimpleDateFormat("EEEE/dd/MM/yyyy");
            
            // //Tanggal Lahirku
            // Calendar cal2 = Calendar.getInstance();
            // cal2.add(Calendar.DAY_OF_MONTH, -6152);
            // System.out.println("tanggal Lahirku : "+sdf.format(cal2.getTime()));

            // //Set Tanggal Secara Manual
            // Calendar cal3 = Calendar.getInstance();
            // Date tgl;
            // try {
            //     tgl = sdf.parse("Tuesday/19/08/2003");
            //     cal3.setTime(tgl);
            //     cal3.add(Calendar.DAY_OF_MONTH, 6152);
            //     System.out.println("Tanggal Hari setelah aku Lahir : "+sdf.format(cal3.getTime()));
            // } catch (ParseException e) {
            //     // TODO Auto-generated catch block
            //     e.printStackTrace();
            // }


        // SimpleDateFormat waktuKu = new SimpleDateFormat("dd/MM/yyyy");

        // //Set Tanggal Secara Manual
        // Calendar lahirKu = Calendar.getInstance();
        // Date tgl;
        // // try {
        //     tgl = waktuKu.parse("19/08/2003");
        //     lahirKu.setTime(tgl);
        //     // lahirKu.add(Calendar.getInstance());
        //     System.out.println("Hari tanggal lahirku : "+ lahirKu);
        // // } catch (ParseException e) {
        // //     // TODO Auto-generated catch block
        // //     e.printStackTrace();
        // // }

    
    


// System.out.println("Ok");

// LocalDate newDate = LocalDate.now();
// System.out.println(newDate);

// LocalTime newTime = LocalTime.now();
// System.out.println(newTime);

// LocalDateTime newDateTime = LocalDateTime.now();
// System.out.println(newDateTime);


// DateTimeFormatter newParser = DateTimeFormatter.ofPattern("dd-MM-yyyy");
// String  formattedDate = newDate.format(newParser);
// System.out.println(formattedDate);
// SimpleDateFormat newFormattedDate = new SimpleDateFormat("dd-MM-yyyy").parse(String.valueOf("22-04-1997"));
// System.out.println(newFormattedDate);