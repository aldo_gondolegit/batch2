package com.nexsoft.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class belajarDate {

    public static void main(String[] args) {

        LocalDate hariIni = LocalDate.of(2020,Month.JUNE,23);
        LocalDate lalu = LocalDate.of(2019,Month.JUNE,23);
        long selisih = ChronoUnit.DAYS.between(lalu, hariIni);  
        System.out.println(selisih);

        DateTimeFormatter formatJam = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime time = LocalTime.now();
        String jamTime = time.format(formatJam);
        System.out.println(jamTime);

    }
    
}