package com.nexsoft.date;

import java.util.Calendar;
import java.util.Scanner;

public class runExamDate extends examDate {

    public static void main(String[] args) {

        runExamDate myOwnCal = new runExamDate();
        myOwnCal.setYear(2020);
        int Y = myOwnCal.getYear();

        int startDayOfMonth = 3;
        int spaces = startDayOfMonth;

        // membuat array months[i] = name of month i
        String[] months = { "", // leave empty so that we start with months[1] = "January"
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
                "November", "December" };

        // days[i] = number of days in month i
        int[] days = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        int[] days1Minggu = { 0, 7};

        for (int M = 1; M <= 12; M++) {

            // check for leap year
            if ((((Y % 4 == 0) && (Y % 100 != 0)) || (Y % 400 == 0)) && M == 2)
                days[M] = 29;

            // print calendar header
            // Display the month and year
            System.out.println("          " + months[M] + " " + Y);

            // Display the lines
            System.out.println("_____________________________________");
            System.out.println(" Sun  Mon  Tue  Wed  Thu  Fri  Sat");

            // spaces required
            spaces = (days[M - 1] + spaces) % 7;

            // print the calendar
            for (int i = 0; i < spaces; i++)
                System.out.print("  -- ");
            for (int i = 1; i <= days[M]; i++) {
                System.out.printf(" %3d ", i);
                if (((i + spaces) % 7 == 0) || (i == days[M]))
                    System.out.println();
            }

            System.out.println();
        }

        myOwnCal.setMonth(5);
        int Z = myOwnCal.getMonth();

        System.out.println("\n =============================================");
        System.out.println("\n HASIL AMBIL HANYA SATU BULAN");
        System.out.println("\n ============================================= \n");

        for (int M = Z; M == Z; M++) {
            System.out.println("          " + months[Z] + " " + Y);
            // Display the lines
            System.out.println("_____________________________________");
            System.out.println(" Sun  Mon  Tue  Wed  Thu  Fri  Sat");

            // spaces required
            spaces = (days[M - 0] + spaces) % 7;

            for (int i = 0; i < spaces; i++)
                System.out.print("  -- ");
            for (int i = 1; i <= days[M]; i++) {
                System.out.printf(" %3d ", i);
                if (((i + spaces) % 7 == 0) || (i == days[M]))
                    System.out.println();
            }

            System.out.println();
        }
        
        System.out.println("\n =============================================");
        System.out.println("\n HASIL AMBIL HANYA SATU MINGGU DI SALAH SATU BULAN");
        System.out.println("\n ============================================= \n");


        // myOwnCal.setMonth(5);
        // int Z = myOwnCal.getMonth();
        myOwnCal.setWeek(1);
        int X = myOwnCal.getWeek();

        for (int M = Z; M == Z; Z++) {
            System.out.println("          " + months[Z] + " " + Y);
            // Display the lines
            System.out.println("_____________________________________");
            System.out.println(" Sun  Mon  Tue  Wed  Thu  Fri  Sat");
    
            // spaces required
            spaces = (days[X - 0] + spaces) % 7;

            for (int i = 0; i <=4; i++)
                System.out.print("  -- ");
            for (int i = 1; i <= days1Minggu[X]; i++) {
                System.out.printf(" %3d ", i);
                if (((i + 4) % 6 == 0) || (i == days1Minggu[X]))
                    System.out.println();
            }
    
            System.out.println();
        }
    }
    
}